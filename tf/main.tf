resource "azurerm_resource_group" "this" {
  name     = var.project_name
  location = var.location
}

resource "azurerm_virtual_network" "this" {
  name                = "network"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.this.location
  resource_group_name = azurerm_resource_group.this.name
}

resource "azurerm_subnet" "private" {
  name                 = "private"
  resource_group_name  = azurerm_resource_group.this.name
  virtual_network_name = azurerm_virtual_network.this.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_subnet" "public" {
  name                 = "public"
  resource_group_name  = azurerm_resource_group.this.name
  virtual_network_name = azurerm_virtual_network.this.name
  address_prefixes     = ["10.0.1.0/24"]
}

resource "random_id" "id" {
  byte_length = 4
}

resource "azurerm_storage_account" "this" {
  name                      = "${var.project_name}${var.stage}${random_id.id.hex}"
  resource_group_name       = azurerm_resource_group.this.name
  location                  = var.location
  account_tier              = "Standard"
  account_replication_type  = "LRS"
  enable_https_traffic_only = true
}

resource "azurerm_storage_share" "this" {
  name                 = var.azure_file_share.name
  storage_account_name = azurerm_storage_account.this.name
  quota                = 50
}

resource "azurerm_storage_share_file" "this" {
  name             = "up.zip"
  storage_share_id = azurerm_storage_share.this.id
  source           = "../docker-assets.zip"
}